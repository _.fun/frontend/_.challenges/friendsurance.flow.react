import Flow from 'components/flow'
import Profile from 'components/profile'

export const configuration = {
  redirect: '/flow'
}

export const routes = [
  {
    name: 'flow',
    title: 'flow',
    path: '/flow',
    component: Flow
  },
  {
    name: 'profile',
    title: 'profile',
    path: '/profile',
    component: Profile
  }
]
