import {
  FLOW_DATA_UPDATE,
  FLOW_UPDATE,
  FLOW_CALCULATE,
  FLOW_LOADING,
  FLOW_ADD_NODE,
  FLOW_DELETE_NODE
} from './constants'

let index = 0

const addNewNode = () => {
  index += 1
  return {
    index,
    id: 'new node',
    body: '',
    trueId: '',
    falseId: '',
    status: null
  }
}

export const initialState = {
  data: '{}',
  list: [
    {
      index,
      id: 'main rule',
      body: '',
      trueId: '',
      falseId: '',
      status: null
    }
  ],
  result: [],
  loading: false
}

export default (state = initialState, { type, value }) => {
  switch (type) {
    case FLOW_LOADING:
      return {
        ...state,
        loading: value
      }
    case FLOW_DATA_UPDATE:
      return {
        ...state,
        data: value
      }
    case FLOW_UPDATE:
      const { id, data } = value
      const { list } = state
      return {
        ...state,
        list: list.map(item => (item.id === id ? { ...item, ...data } : item))
      }
    case FLOW_CALCULATE:
      return {
        ...state,
        ...value
      }
    case FLOW_ADD_NODE:
      return {
        ...state,
        list: [...state.list, addNewNode()]
      }
    case FLOW_DELETE_NODE:
      return {
        ...state,
        list: state.list.filter(({ index }) => index !== value)
      }
    default:
      return state
  }
}
