import React from 'react'
import PropTypes from 'prop-types'

import { Grid } from './containers'

const Result = ({ value }) => (
  <Grid>
    <span>result: {value.join(' -> ')}</span>
  </Grid>
)

export const ResultPropTypes = {
  value: PropTypes.array.isRequired
}

Result.propTypes = ResultPropTypes

export default Result
