import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'components/common'

import { Grid } from './containers'

const handleChange = action => ({ target: { value } }) => action(value)

const Data = ({ value, updateAction }) => (
  <Grid>
    <span>data:</span>
    <Input
      isStretch
      placeholder="data"
      value={value}
      name="body"
      onChange={handleChange(updateAction)}
    />
  </Grid>
)

export const DataPropTypes = {
  value: PropTypes.string,
  updateAction: PropTypes.func.isRequired
}

Data.propTypes = DataPropTypes

export default Data
