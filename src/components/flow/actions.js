import {
  FLOW_DATA_UPDATE,
  FLOW_UPDATE,
  FLOW_ADD_NODE,
  FLOW_DELETE_NODE,
  FLOW_CALCULATE
} from './constants'

export const dataUpdate = value => ({
  type: FLOW_DATA_UPDATE,
  value
})

export const update = value => ({
  type: FLOW_UPDATE,
  value
})

export const addNode = () => ({
  type: FLOW_ADD_NODE
})

export const deleteNode = value => ({
  type: FLOW_DELETE_NODE,
  value
})

export const calculate = value => ({
  type: FLOW_CALCULATE,
  value
})

export const dataUpdateAction = value => dispatch => dispatch(dataUpdate(value))

export const updateAction = value => dispatch => dispatch(update(value))

export const addNodeAction = () => dispatch => dispatch(addNode())

export const deleteNodeAction = value => dispatch => dispatch(deleteNode(value))

export const calculateAction = () => (dispatch, getState) =>
  dispatch(calculation(getState()))

const calculation = ({ flow: { data, list } }) => {
  list.forEach(item => (item.status = null))

  const result = []
  const dataParsed = JSON.parse(data)
  if (!dataParsed) return calculate({ result, list })
  if (!list || list.length === 0) return calculate({ result, list })

  let currentItem = list[0]
  while (currentItem) {
    const { id, body, trueId, falseId } = currentItem
    result.push(id)

    const bodyResult = calculateBody(body, data)
    if (bodyResult === null) {
      result.push('[error] [exit]')
      break
    }

    let nextId = ''
    if (bodyResult) {
      nextId = trueId
      currentItem.status = true
    } else {
      nextId = falseId
      currentItem.status = false
    }

    if (nextId) currentItem = list.find(element => element.id === nextId)
    else currentItem = null

    if (currentItem && currentItem.status !== null) {
      currentItem = null
      result.push('[loop] [exit]')
    }
  }

  return calculate({ result, list })
}

const calculateBody = (body, data) => {
  const bodyEval = eval(body)
  if (typeof bodyEval !== 'function') return null

  return bodyEval(data)
}
