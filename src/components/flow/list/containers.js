import styled from 'styled-components'

export const Grid = styled.div`
  grid-area: list;

  display: grid;
  grid-template: 'items' 'action';
  grid-gap: 1rem;
`

export const Items = styled.div`
  grid-area: items;

  display: grid;
  grid-gap: 1rem;
  grid-auto-flow: row;
`
