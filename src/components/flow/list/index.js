import React from 'react'
import PropTypes from 'prop-types'

import { Grid, Items } from './containers'
import Loading from './loading'
import Empty from './empty'
import Item, { ItemModelPropType, ItemComponentPropType } from './item'
import Action, { ActionPropTypes } from './action'

const List = ({
  list,
  loading,
  updateAction,
  deleteNodeAction,
  addNodeAction
}) => {
  if (loading) return <Loading />
  if (list.length === 0) return <Empty addNodeAction={addNodeAction} />

  return (
    <Grid>
      <Items>
        {list.map(item => (
          <Item
            key={item.index}
            {...item}
            updateAction={updateAction}
            deleteNodeAction={deleteNodeAction}
          />
        ))}
      </Items>
      <Action addNodeAction={addNodeAction} />
    </Grid>
  )
}

export const ListPropTypes = {
  loading: PropTypes.bool.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape(ItemModelPropType)).isRequired,
  ...ItemComponentPropType,
  ...ActionPropTypes
}

List.propTypes = ListPropTypes

export default List
