import styled from 'styled-components'

export const Grid = styled.div`
  grid-area: action;

  display: grid;
  grid-gap: 1rem;
  grid-auto-flow: row;
`
