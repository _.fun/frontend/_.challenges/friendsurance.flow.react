import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'components/common'

import { Grid } from './containers'

const Action = ({ addNodeAction }) => (
  <Grid>
    <Button isCircular onClick={addNodeAction}>
      Add node
    </Button>
  </Grid>
)

export const ActionPropTypes = {
  addNodeAction: PropTypes.func.isRequired
}

Action.propTypes = ActionPropTypes

export default Action
