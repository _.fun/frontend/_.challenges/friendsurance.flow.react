import React from 'react'

import { Grid, Items } from './containers'
import Action from './action'

export default ({ addNodeAction }) => (
  <Grid>
    <Items>Empty</Items>
    <Action addNodeAction={addNodeAction} />
  </Grid>
)
