import styled, { css } from 'styled-components'
import { darken } from 'polished'

export const Grid = styled.div`
  display: grid;
  padding: 1em;

  grid-gap: 1rem;

  grid-template:
    'status action'
    'id id'
    'body body'
    'true false';

  ${({ theme: { background } }) => css`
    background-color: ${darken(0.05, background)};
  `};
`

/* eslint-disable */
export const Status = styled.div`
  grid-area: status;

  padding: 1rem;

  ${({ status }) => css`
    background-color: ${status === true
      ? 'green'
      : status === false
        ? 'red'
        : 'white'};
  `};
`
/* eslint-enabled */

export const Action = styled.div`
  grid-area: action;
`

export const Id = styled.div`
  grid-area: id;
`

export const Body = styled.div`
  grid-area: body;
`

export const True = styled.div`
  grid-area: true;
`

export const False = styled.div`
  grid-area: false;
`
