import React from 'react'

import { Block } from 'components/common/loading'
import { Grid, Status, Id, Body, True, False } from './containers'

export default () => (
  <Grid>
    <Status>
      <Block />
    </Status>
    <Id>
      <Block />
    </Id>
    <Body>
      <Block />
    </Body>
    <True>
      <Block />
    </True>
    <False>
      <Block />
    </False>
  </Grid>
)
