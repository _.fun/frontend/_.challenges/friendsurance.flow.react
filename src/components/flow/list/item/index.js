import React from 'react'
import PropTypes from 'prop-types'
import { Button, Input } from 'components/common'

import { Grid, Status, Action, Id, Body, True, False } from './containers'

const handleChange = action => id => ({ target: { name, value } }) =>
  action({ id, data: { [name]: value } })

const Item = ({
  index,
  id,
  body,
  trueId,
  falseId,
  status,
  updateAction,
  deleteNodeAction
}) => (
  <Grid>
    <Status status={status}>
      <span>
        Status:{' '}
        {status === true ? 'passed' : status === false ? 'failed' : 'skipped'}
      </span>
    </Status>
    <Action>
      <Button isCircular onClick={() => deleteNodeAction(index)}>
        Delete Node
      </Button>
    </Action>
    <Id>
      <span>id:</span>
      <Input
        isStretch
        placeholder="Id"
        value={id}
        name="id"
        onChange={handleChange(updateAction)(id)}
      />
    </Id>
    <Body>
      <span>body:</span>
      <Input
        isStretch
        placeholder="body"
        value={body}
        name="body"
        onChange={handleChange(updateAction)(id)}
      />
    </Body>
    <True>
      <span>true id:</span>
      <Input
        isStretch
        placeholder="true id"
        value={trueId}
        name="trueId"
        onChange={handleChange(updateAction)(id)}
      />
    </True>
    <False>
      <span>false id:</span>
      <Input
        isStretch
        placeholder="false id"
        value={falseId}
        name="falseId"
        onChange={handleChange(updateAction)(id)}
      />
    </False>
  </Grid>
)

export const ItemComponentPropType = {
  updateAction: PropTypes.func.isRequired,
  deleteNodeAction: PropTypes.func.isRequired
}

export const ItemModelPropType = {
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  trueId: PropTypes.string,
  falseId: PropTypes.string,
  status: PropTypes.bool
}

Item.propTypes = { ...ItemModelPropType, ...ItemComponentPropType }

export default Item
