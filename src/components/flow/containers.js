import styled from 'styled-components'

export const Grid = styled.div`
  display: grid;

  grid-template: 'data' 'list' 'action' 'result';
  grid-gap: 1rem;

  align-items: center;
`
