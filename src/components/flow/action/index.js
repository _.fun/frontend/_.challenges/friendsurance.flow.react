import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'components/common'

import { Grid } from './containers'

const Action = ({ calculateAction }) => (
  <Grid>
    <Button isCircular onClick={calculateAction}>
      Calculate
    </Button>
  </Grid>
)

export const ActionPropTypes = {
  calculateAction: PropTypes.func.isRequired
}

Action.propTypes = ActionPropTypes

export default Action
