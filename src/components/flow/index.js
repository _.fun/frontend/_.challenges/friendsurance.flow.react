import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Grid } from './containers'
import Data from './data'
import List, { ListPropTypes } from './list'
import Action, { ActionPropTypes } from './action'
import Result from './result'

import {
  addNodeAction,
  deleteNodeAction,
  dataUpdateAction,
  updateAction,
  calculateAction
} from './actions'

const Flow = ({
  data,
  result,
  list,
  loading,
  addNodeAction,
  deleteNodeAction,
  updateAction,
  dataUpdateAction,
  calculateAction
}) => (
  <Grid>
    <Data value={data} updateAction={dataUpdateAction} />
    <List
      list={list}
      loading={loading}
      updateAction={updateAction}
      deleteNodeAction={deleteNodeAction}
      addNodeAction={addNodeAction}
    />
    <Action calculateAction={calculateAction} />
    <Result value={result} />
  </Grid>
)

Flow.propTypes = {
  data: PropTypes.string,
  dataUpdateAction: PropTypes.func.isRequired,
  result: PropTypes.array.isRequired,
  ...ListPropTypes,
  ...ActionPropTypes
}

const mapStateToProps = ({ flow: { data, result, list, loading } }) => ({
  data,
  result,
  list,
  loading
})
const mapActionsToProps = {
  addNodeAction,
  deleteNodeAction,
  dataUpdateAction,
  updateAction,
  calculateAction
}

export default connect(mapStateToProps, mapActionsToProps)(Flow)
